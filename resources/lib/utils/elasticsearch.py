from resources.lib.const import FILTER_CONFIG_OPERATOR, FILTER_CONFIG_ITEM_VALUE_TYPE, FILTER_CONFIG_ITEM_TYPE, \
    FILTER_CONFIG_CONDITION

OPERATORS = {
    "not": lambda query: {
        "bool": {
            "must_not": query
        }
    },
}

CUSTOM_FILTER_BASE_STRING = {}
# CONTAINS
CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.CONTAINS] = lambda item, fields: {
    "query_string": {
        "fields": fields,
        "query": '*%s*' % item["value"]
    }
}
CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.NOT_CONTAINS] = lambda item, fields: OPERATORS["not"](
    CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.NOT_CONTAINS])
#
# EQUAL
CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.EQUAL] = lambda item, fields: {
    "query_string": {
        "fields": fields,
        "query": '%s' % item["value"]
    }
}
CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.NOT_EQUAL] = lambda item, fields: OPERATORS["not"](
    CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.EQUAL])
#
# STARTS_WITH
CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.STARTS_WITH] = lambda item, fields: {
    "bool": {
        "should": [{"prefix": {field: item["value"]}} for field in fields],
        "minimum_should_match": 1
    }
}
CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.NOT_STARTS_WITH] = OPERATORS["not"](
    CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.STARTS_WITH])
#
# ENDS_WITH
CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.ENDS_WITH] = lambda item, fields: {
    "query_string": {
        "fields": fields,
        "query": '*%s' % item["value"]
    }
}
CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.NOT_ENDS_WITH] = OPERATORS["not"](
    CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.ENDS_WITH])
#

CUSTOM_FILTER_BASE_NUMBER = {}
# EQUAL
CUSTOM_FILTER_BASE_NUMBER[FILTER_CONFIG_OPERATOR.EQUAL] = lambda item, fields: {
    "bool": {
        "should": [{"term": {field: item["value"]}} for field in fields],
        "minimum_should_match": 1
    }
}
CUSTOM_FILTER_BASE_NUMBER[FILTER_CONFIG_OPERATOR.NOT_EQUAL] = lambda item, fields: OPERATORS["not"](
    CUSTOM_FILTER_BASE_STRING[FILTER_CONFIG_OPERATOR.EQUAL])
#
# HIGHER
CUSTOM_FILTER_BASE_NUMBER[FILTER_CONFIG_OPERATOR.HIGHER] = lambda item, fields: {
    "bool": {
        "should": [{"range": {field: {"gt": item["value"]}}} for field in fields],
        "minimum_should_match": 1
    }
}
#
# HIGHER_OR_EQUAL
CUSTOM_FILTER_BASE_NUMBER[FILTER_CONFIG_OPERATOR.HIGHER_OR_EQUAL] = lambda item, fields: {
    "bool": {
        "should": [{"range": {field: {"gte": item["value"]}}} for field in fields],
        "minimum_should_match": 1
    }
}
#
# LOWER
CUSTOM_FILTER_BASE_NUMBER[FILTER_CONFIG_OPERATOR.LOWER] = lambda item, fields: {
    "bool": {
        "should": [{"range": {field: {"lt": item["value"]}}} for field in fields],
        "minimum_should_match": 1
    }
}
#
# LOWER_OR_EQUAL
CUSTOM_FILTER_BASE_NUMBER[FILTER_CONFIG_OPERATOR.LOWER_OR_EQUAL] = lambda item, fields: {
    "bool": {
        "should": [{"range": {field: {"lte": item["value"]}}} for field in fields],
        "minimum_should_match": 1
    }
}
#

CUSTOM_FILTER_BASE = {
    FILTER_CONFIG_ITEM_VALUE_TYPE.STRING: CUSTOM_FILTER_BASE_STRING,
    FILTER_CONFIG_ITEM_VALUE_TYPE.SELECT: CUSTOM_FILTER_BASE_STRING,
    FILTER_CONFIG_ITEM_VALUE_TYPE.NUMBER: CUSTOM_FILTER_BASE_NUMBER,
}

CUSTOM_FILTER_FIELDS = {
    FILTER_CONFIG_ITEM_TYPE.TITLE: ["i18n_info_labels.title", "info_labels.originaltitle"],
    FILTER_CONFIG_ITEM_TYPE.YEAR: ["info_labels.year"],
    FILTER_CONFIG_ITEM_TYPE.MEDIA_TYPE: ["info_labels.mediatype"],
}


def get_query(item):
    item_type = item["type"]
    value_type = item["value_type"]
    fields = CUSTOM_FILTER_FIELDS[item_type]
    return CUSTOM_FILTER_BASE[value_type][item["comparison"]](item, fields)


def get_base_query():
    return {
        "bool": {
            "must": [
                {
                    "bool": {
                        "should": [

                        ]
                    }
                }
            ],
            "must_not": [],
            "should": [],
            "minimum_should_match": 0,
            "boost": 1.0
        }
    }


def push_to_bool_query(query, operator, q):
    if operator == FILTER_CONFIG_CONDITION.AND:
        query["must"].append(q)
    elif operator == FILTER_CONFIG_CONDITION.OR:
        query["should"].append(q)
    elif operator == FILTER_CONFIG_CONDITION.NOT_AND:
        query["must_not"].append(q)
    elif operator == FILTER_CONFIG_CONDITION.NOT_OR:
        query["must"][1]["bool"]["should"].append({
            "bool": {
                "must_not": q
            }
        })
