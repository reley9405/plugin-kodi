import re

from resources.lib.const import ICON_PATH, ICON
from resources.lib.utils.kodiutils import get_icon


class IconRenderer:
    @staticmethod
    def _get_icon(path):
        return get_icon(path)

    @staticmethod
    def default(name):
        return IconRenderer._get_icon(name)

    @staticmethod
    def none(item):
        return None

    @staticmethod
    def country(item_name):
        return IconRenderer._get_icon(ICON_PATH.COUNTRY.format(re.sub(r'[\W_]+', '-', item_name).lower()))

    @staticmethod
    def tv(item_name):
        return IconRenderer._get_icon(ICON_PATH.TV.format(item_name))

    @staticmethod
    def _quality(path, item_name, is_hdr=False, is_3d=False):
        icon_name = item_name + ('-DV' if is_hdr == "Dolby Vision" else ('-HDR' if is_hdr else ''))
        icon_name = icon_name + ('-3D' if is_3d else '')
        return IconRenderer._get_icon(path.format(icon_name))

    @staticmethod
    def quality(item_name, is_hdr=False, is_3d=False):
        return IconRenderer._quality(ICON_PATH.QUALITY, item_name, is_hdr, is_3d)

    @staticmethod
    def quality_no_gap(item_name, is_hdr=False):
        return IconRenderer._quality(ICON_PATH.QUALITY_NO_GAP, item_name, is_hdr)

    @staticmethod
    def a_z(*args):
        return IconRenderer._get_icon(ICON.A_Z_2)
